////  SwiftUI2_ScrollableTabBarWithSmoothScrollingTabEffectApp.swift
//  SwiftUI2_ScrollableTabBarWithSmoothScrollingTabEffect
//
//  Created on 11/02/2021.
//  
//

import SwiftUI

@main
struct SwiftUI2_ScrollableTabBarWithSmoothScrollingTabEffectApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
