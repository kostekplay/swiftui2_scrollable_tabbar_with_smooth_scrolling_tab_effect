////  HomeView.swift
//  SwiftUI2_ScrollableTabBarWithSmoothScrollingTabEffect
//
//  Created on 11/02/2021.
//  
//

import SwiftUI
import SDWebImageSwiftUI

struct HomeView: View {
    
    @State private var offset: CGFloat = 0
    @State private var showCapsule = false
    
    var body: some View {
        
        GeometryReader{ geo in
            
            let rect = geo.frame(in: .global)
            
            ScrollableView(tabs: tabs, rect: rect, offset: $offset) {
                
                HStack(spacing: 0, content: {
                    
                    ForEach(imageURLs.indices, id: \.self) { index in
                        
                        WebImage(url: URL(string: imageURLs[index]))
                            .resizable()
                            .scaledToFill()
                            .frame(width: rect.width)
                            .cornerRadius(4)
                            .overlay(Color.black.opacity(0.2))
                        
                    }//: ForEach
                    
                })//: HStack
                .ignoresSafeArea()
            } //: ScrollableView
            
        }//: GeometryReader
        .ignoresSafeArea()
        .overlay(
            TabBarView(offset: $offset,showCapsule: $showCapsule),
            alignment: .top
        )
        .overlay(
            Button(action: {
                withAnimation{showCapsule.toggle()}
            }, label: {
                Image(systemName: "fiberchannel")
                    .font(.title2)
                    .padding()
                    .background(Color("lightblue"))
                    .foregroundColor(.black)
                    .clipShape(Circle())
            })
            .padding()
            ,alignment: .bottomTrailing
        )
    }
}

var tabs = ["Home", "Search", "Account"]

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
