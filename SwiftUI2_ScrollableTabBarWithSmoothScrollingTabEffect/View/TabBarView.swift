////  TabBarView.swift
//  SwiftUI2_ScrollableTabBarWithSmoothScrollingTabEffect
//
//  Created on 11/02/2021.
//  
//

import SwiftUI

struct TabBarView: View {
    
    @Binding var offset: CGFloat
    @Binding var showCapsule: Bool
    
    @State var width : CGFloat = 0
    
    var body: some View {
        
        GeometryReader{ geo -> AnyView in
            
            let equalWidth = geo.frame(in: .global).width / CGFloat(tabs.count)
            
            DispatchQueue.main.async {
                self.width = equalWidth
            }
            
            return AnyView(
            
                ZStack(alignment: .bottomLeading, content: {
                    
                    Capsule()
                        .fill(Color("lightblue"))
                        .frame(width: equalWidth - 15, height: showCapsule ? 40 : 4)
                        .offset(x: getOffset() + 5)
                      
                    HStack(spacing: 0, content: {
                        
                        ForEach(tabs.indices, id: \.self) { index in
                            Text(tabs[index])
                                .fontWeight(.bold)
                                .foregroundColor(showCapsule ? (getIndexFromOffset() == CGFloat(index) ? .black : .white) : .white)
                                .frame(width: equalWidth, height: 40)
                                //.contentShape(Rectangle())
                                .onTapGesture {
                                    withAnimation {
                                        offset = UIScreen.main.bounds.width * CGFloat(index)
                                    }
                                }
                                
                        } //:ForEach
                        
                    }) //:HStack
                    
                }) //: ZStack
                //TODO: .frame(maxWidth: .infinity, maxHeight: 40, alignment: .center)
                //TODO: .clipShape(Capsule())
                
            ) //:AnyView
            
        } //:GeometryReader
        //TODO: .padding()
        //TODO: .frame(height: 40)
        
    }
    
    func getOffset() -> CGFloat{
        let progress = offset / UIScreen.main.bounds.width
        return progress * width
    }
    
    func getIndexFromOffset()->CGFloat{
        let indexFloat = offset / UIScreen.main.bounds.width
        return indexFloat.rounded(.toNearestOrAwayFromZero)
    }
    
}

struct TabBarView_Previews: PreviewProvider {
    static var previews: some View {
        //TabBarView(offset: Binding<CGFloat>, showCapsule: Binding<Bool>)
        HomeView()
    }
}
