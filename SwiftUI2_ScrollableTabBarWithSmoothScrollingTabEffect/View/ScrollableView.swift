////  ScrollableView.swift
//  SwiftUI2_ScrollableTabBarWithSmoothScrollingTabEffect
//
//  Created on 11/02/2021.
//  
//

import SwiftUI

struct ScrollableView<Content: View>: UIViewRepresentable {
    
    var tabs: [Any]
    
    // calulate width and height of scrollView
    var rect: CGRect
    
    // ContentOffset...
    @Binding private var offset: CGFloat
    
    // store SwiftUI View
    var content: Content
    
    let scrollView = UIScrollView()
    
    internal init(tabs: [Any], rect: CGRect,  offset: Binding<CGFloat>, content: ()-> Content) {
        self.tabs = tabs
        self.rect = rect
        self._offset = offset
        self.content = content()
    }
    
    func makeCoordinator() -> Coordinator {
        return ScrollableView.Coordinator(parent: self)
    }
    
    func makeUIView(context: Context) ->  UIScrollView {
        
        setUpScrollView()
        
        // setting Content Size...
        scrollView.contentSize = CGSize(width: rect.width * CGFloat(tabs.count), height: rect.height)
        scrollView.addSubview(extractView())
        scrollView.delegate = context.coordinator
        
        return scrollView
    }
    
    func updateUIView(_ uiView: UIScrollView, context: Context) {
        
        // Updating View...
        if uiView.contentOffset.x != offset{
            
            // Animating...
            // The ANimation Glitch Is Because It s Updating On Two Times...
            
            // Simple....
            // Removing Delegate While Animating...
            
            uiView.delegate = nil
            
            UIView.animate(withDuration: 0.4) {
                uiView.contentOffset.x = offset
            } completion: { (status) in
                if status{uiView.delegate = context.coordinator}
            }
        }
    }
    
    func setUpScrollView(){
        
        scrollView.isPagingEnabled = true
        scrollView.bounces = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
    }
    
    func extractView()->UIView{
        
        let controller = UIHostingController(rootView: content)
        controller.view.frame = CGRect(x: 0, y: 0, width: rect.width * CGFloat(tabs.count), height: rect.height)
        return controller.view!
    }
    
    // Delegate Function To Get Offset...
    class Coordinator: NSObject,UIScrollViewDelegate{
        
        var parent: ScrollableView
        
        init(parent: ScrollableView) {
            self.parent = parent
        }
        
        func scrollViewDidScroll(_ scrollView: UIScrollView) {
            parent.offset = scrollView.contentOffset.x
        }
    }
}

struct ScrollableView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
